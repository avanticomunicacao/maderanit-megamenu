<?php
namespace Ibnab\MegaMenu\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Event\ObserverInterface;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Store\Model\ScopeInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\Config\ScopeConfigInterface;

class AddNode implements ObserverInterface
{
    const DEFAULT_CATEGORY_ID = 2;

    protected $categoryRepository;
    protected $scopeConfig;


    public function __construct(
        CategoryRepository $categoryRepository,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @inheritDoc
     */
    public function execute(EventObserver $observer)
    {
        $menuNode = $this->addRootCategory($observer);
        $allCategories = $this->getAllRootCategories();
        $this->addChildCategories($observer, $menuNode, $allCategories);
    }

    public function addRootCategory($observer)
    {
        /** @var \Magento\Framework\Data\Tree\Node $menu */
        $menu = $observer->getMenu();
        $teste = $menu->getAllChildNodes();
        $tree = $menu->getTree();
        $data = [
            'name'      => __('Ver Mais'),
            'id'        => 'category-node-1',
            'url'       => '#',
            'is_active' => (1),
            'category_is_link' => true,
            'is_parent_active' => true,
            'use_static_block_right' => 1,
            'static_block_right_value' => $this->getRighBlock() ?? null
        ];
        $menuNode = new Node($data, 'id', $tree, $menu);
        $menu->addChild($menuNode);
        $this->sortOrderMenu($menu);
        return $menuNode;
    }

    /** @var \Magento\Framework\Data\Tree\Node $menu */
    public function sortOrderMenu($menu)
    {
        $nodes = $menu->getChildren()->getNodes();
        ksort($nodes);
        foreach ($nodes as $node) {
            $menu->removeChild($node);
        }

        foreach ($nodes as $node) {
            $tree = $menu->getTree();
            $menu->addChild($node);
        }
        return $menu;
    }

    public function addChildCategories($observer, $menuNode, $allCategories)
    {
        $tree = $menuNode->getTree();
        $categories = $allCategories;

        $count = 0;
        foreach ($categories as $category) {
            $count += 1;
            $data = [
                'name' => __($category->getName()),
                'id' => 'root-categories-'.$count,
                'url' => $category->getUrl(),
                'is_active' => (1),
                'category_is_link' => true,
                'is_parent_active' => true
            ];
            $node = new Node($data, 'id', $tree, $menuNode);
            $menuNode->addChild($node);
        }
    }


    public function child2($observer, $menuNode, $allCategories)
    {
        $tree = $menuNode->getTree();
        $categories = $allCategories;

        $count = 0;
        foreach ($categories as $category) {
            $count += 1;
            $data = [
                'name' => __($category->getName()),
                'id' => 'subroot-categories' . $count,
                'url' => $category->getUrl(),
                'is_active' => (1),
                'category_is_link' => true,
                'is_parent_active' => true
            ];
            $node = new Node($data, 'id', $tree, $menuNode);
            $menuNode->addChild($node);
        }
    }

    public function getAllRootCategories()
    {
        $category = $this->categoryRepository->get(self::DEFAULT_CATEGORY_ID);
        return $category->getChildrenCategories();
    }

    public function getRighBlock()
    {
        $storeScope = ScopeInterface::SCOPE_STORES;
        return $this->scopeConfig->getValue('ibnab_mega_config/general/megamenublock',$storeScope);
    }
}